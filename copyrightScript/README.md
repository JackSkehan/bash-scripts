## README

This script is a simple bash program designed to add a code header to files that do not already have one. The header added to the top of the file reads `#Coded by Jack Skehan`. 

### Requirements
you need to be using a Unix shell like Bash or ZSH. If you are on Linux or macOS, you don’t have to do anything special. If you are on Windows, you need to make sure you are not running cmd.exe or PowerShell; you can use Windows Subsystem for Linux or a Linux virtual machine to use Unix-style command-line tools. To make sure you’re running an appropriate shell, you can try the command echo $SHELL. If it says something like /bin/bash or /usr/bin/zsh, that means you’re running the right program.

### How to Use
To use the script, simply run it in your terminal with the names of the files you want to add a header to as arguments. For example, to add the header to `file1.py` and `file2.py`, run:

```     
./copyright-script 2 4 6 
```


The script will then check each file for the presence of the header. If the header is not found, it will be added to the top of the file. 

### Notes
- The script uses `grep` to search for the header in each file. If the header is not found, it is added to the top of the file using `sed`.
- The script redirects STDOUT and STDERR to `/dev/null` to suppress output. 
- The script outputs some information to the terminal when it is run, including the date, the name of the script, the number of arguments, and the process ID.
