#!/bin/bash
gclone() {

        if [ $# -eq 0 ]; then
            echo "Please enter repo name or full url:";
            read repo;
        elif [[ $1 == --help ]] || [[ $1 == --h ]] || [[ $1 == --? ]]; then
            echo "This will clone a git repo.";
            echo "";
            echo "Option 1: You can just provide the name, eg:";
            echo "$ clone membership";
            echo "This will do: git clone https://gitlab.com/JackSkehan/membership.git";
            echo "";
            echo "Option 2: Provide the full URL";
            echo "$ clone https://gitlab.com/smallrye/smallrye-rest-client.git";
            echo "This will do: git clone https://gitlab.com/smallrye/smallrye-rest-client.git";
        else    
            if [[ $1 == https://* ]] || [[ $1 == git://* ]] || [[ $1 == ssh://* ]] ; then
                URL=$1;
            else
                URL='https://gitlab.com/JackSkehan/'$1'.git';
            fi    

            echo git clone "$URL";
            git clone "$URL";
        
        fi
    }



gsync() {
    
    if git remote -v | grep -q 'upstream'; then
        echo "upstream exist";
    else
        echo "Please enter the upstream git url:";
        read url;
        git remote add upstream "$url"
    fi
    
    git remote -v
    git fetch upstream
    git pull upstream master
    git checkout master
    git rebase upstream/master
}

gcommit() {
    
    if [ $# -eq 0 ]; then
        echo "Please enter a commit message:";
        read msg;
        elif [[ $1 == --help ]] || [[ $1 == --h ]] || [[ $1 == --? ]]; then
        echo "This will commit changes to a local git repo, eg:";
        echo "$ commit 'some changes made'";
        echo "This will do: git commit -s -m 'some changes made'";
    else
        echo git commit -s -a -m "$1"
        git commit -s -a -m "$1";
    fi
}

grebase() {
    git fetch upstream
    git rebase upstream/master
    git push -f
}
