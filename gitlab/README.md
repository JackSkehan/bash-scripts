# Readme for Gitlabs bash Functions

This bash script contains 4 functions: `gclone`, `gsync`, `gcommit`, and `grebase`.

## gclone()

This function is used for cloning Git repositories. It takes one argument, which can either be the name of the repository or the full URL of the repository. If no argument is provided, it prompts the user to enter the name or URL. If the argument is a URL, it clones that repository. Otherwise, it constructs a URL by appending the argument to `https://gitlab.com/JackSkehan/` and clones that repository. 

### Usage

```bash
$ gclone [repository-name|repository-url]
```

If no argument is provided, the function prompts the user to enter the name or URL of the repository.
```bash
$ gclone
Please enter repo name or full url:
```

If the argument is a valid URL, the function clones the repository.



```bash
$ gclone https://github.com/user/repo.git
```

Otherwise, the function constructs a URL and clones the repository.

```bash
$ gclone repo-name
```

## gsync()
This function is used for syncing a local repository with its upstream repository. It checks if an upstream repository exists, and if not, prompts the user to enter the URL of the upstream repository. It then fetches the upstream changes, pulls the changes into the local repository, checks out the master branch, and rebases the changes.

Usage
```bash
$ gsync
```
gcommit()
This function is used for committing changes to a local Git repository. It takes one argument, which is the commit message. If no argument is provided, it prompts the user to enter a commit message. The -s flag signs the commit and -a flag stages changes.

Usage
```bash
$ gcommit [commit-message]
```
If no argument is provided, the function prompts the user to enter a commit message.

```bash
$ gcommit
```
Please enter a commit message:
Otherwise, the function commits the changes with the provided commit message.

```bash
$ gcommit "Some changes made"
```

## grebase()
This function is used for rebasing a local repository with its upstream repository. It fetches the upstream changes, rebases the changes, and force-pushes the changes to the local repository.

Usage
```bash
$ grebase
```