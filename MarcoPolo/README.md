# Marco Polo Script

The Marco Polo script defines two Bash functions: `marco` and `polo`.

## Usage

To use the script, simply `source` the file in your shell:

```bash
source marco.sh


then simply: 

marco     # save the current directory
cd /path/to/some/other/directory
polo      # return to the directory saved by marco
```
## Functionality

The marco function saves the current directory to an environment variable called MARCO


The polo function changes the current directory to the directory saved in MARCO

## Notes
- The MARCO variable is exported so that it can be accessed by the polo function even though it was defined in a different function.
-The script must be sourced in your shell in order to define the functions and make them available for use.(could add to your bashrc script to this on bootup to avoid doing it everytime)
-The script is compatible with Bash shells.