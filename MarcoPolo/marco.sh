#!/bin/bash

marco() {
  # Save the current directory to the MARCO variable
  export MARCO=$(pwd)
}

polo() {
  # Change the current directory to the one saved in the MARCO variable
  cd "$MARCO"
}
